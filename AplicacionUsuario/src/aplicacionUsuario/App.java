package aplicacionUsuario;

import java.awt.EventQueue;

import javax.swing.JFrame;

import aplicacionUsuario.gui.AbstractView;
import aplicacionUsuario.gui.GuardarView;
import aplicacionUsuario.gui.LoginView;
import aplicacionUsuario.gui.ModificarView;
import aplicacionUsuario.modelo.Usuario;
import aplicacionUsuario.servicio.UsuarioException;

public class App {
	private JFrame frame;
	private LoginView loginView;
	private ModificarView modificarView;
	private GuardarView guardarView;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public App() throws UsuarioException {
		initialize();
	}
	
	private void initialize() throws UsuarioException {
		frame = new JFrame();
		frame.setBounds(100, 100, 245, 200);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		loginView = new LoginView(this);
		frame.setContentPane(loginView);
	}
	
	public void cambiarView(AbstractView view) {
		frame.setContentPane(view);
		view.initialize();
		frame.revalidate();
	}

	public void btnEntrarView(Usuario usuario) throws UsuarioException {
		modificarView = new ModificarView(this, usuario);
		cambiarView(modificarView);
	}
	
	public void btnModificarView(Usuario usuario) throws UsuarioException {
		guardarView = new GuardarView(this, usuario);
		cambiarView(guardarView);
	}
	
	public void volverLoginView() {
		cambiarView(loginView);
	}

	public void volverModificarView(Usuario usuario) throws UsuarioException {
		modificarView = new ModificarView(this, usuario);
		cambiarView(modificarView);
	}
}
