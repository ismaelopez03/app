package aplicacionUsuario.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import aplicacionUsuario.App;
import aplicacionUsuario.modelo.Usuario;
import aplicacionUsuario.servicio.UsuarioException;
import aplicacionUsuario.servicio.UsuarioService;

import javax.swing.JButton;

public class LoginView extends AbstractView {

	private static final long serialVersionUID = -6109054748698849199L;

	private JTextField txtUsuario;
	private JPasswordField txtPassword;
	private UsuarioService servicio;

	public LoginView(App appController) {
		setLayout(null);
		this.appController = appController;

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(10, 11, 77, 14);
		add(lblUsuario);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 36, 77, 14);
		add(lblPassword);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(97, 8, 117, 20);
		add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtUsuario.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) { }
			
			@Override
			public void keyReleased(KeyEvent e) { }
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
//					txtPassword.grabFocus();
				}
				
				else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					
				}
			}
		});

		txtPassword = new JPasswordField();
		txtPassword.setBounds(97, 33, 117, 20);
		add(txtPassword);
		txtPassword.setColumns(10);

		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(26, 128, 89, 23);
		add(btnEntrar);
		btnEntrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				servicio = new UsuarioService();
				Usuario usuario = new Usuario();
				String password = String.valueOf(txtPassword.getPassword());

				if (txtUsuario.getText().isBlank() || txtUsuario.getText().isEmpty()
						|| password.isBlank() || password.isEmpty()) {
					JOptionPane.showMessageDialog(null, "El usuario y/o la contraseña no pueden estar en blanco.");
				} else {
					try {
						
						usuario.setUsuario(txtUsuario.getText());
						usuario.setPassword(password);
						if (servicio.btnLogin(usuario) == true) {
							appController.btnEntrarView(usuario);
							txtUsuario.setText("");
							txtPassword.setText("");
							
						}
						else {
							JOptionPane.showMessageDialog(null, "El usuario y/o la contraseña no estan en la BBDD.");
						}
					} catch (UsuarioException e1) {
						e1.printStackTrace();
					}

				}
			}
		});
		

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(125, 128, 89, 23);
		add(btnSalir);

		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		initialize();
	}


	@Override
	public void initialize() { }
}
