package aplicacionUsuario.gui;

import javax.swing.JPanel;

import aplicacionUsuario.App;

public abstract class AbstractView extends JPanel {

	private static final long serialVersionUID = -5457438671720305260L;
	
	public App appController;
	
	public abstract void initialize();

}
