package aplicacionUsuario.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import aplicacionUsuario.App;
import aplicacionUsuario.modelo.Usuario;
import aplicacionUsuario.servicio.UsuarioException;
import aplicacionUsuario.servicio.UsuarioService;

public class ModificarView extends AbstractView {

	private static final long serialVersionUID = -4164478213908680555L;

	private JTextField txtUsername;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	private UsuarioService servicio;

	public ModificarView(App appController, Usuario usuario) throws UsuarioException {
		setLayout(null);
		this.appController = appController;

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 11, 97, 14);
		add(lblUsername);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 36, 97, 14);
		add(lblNombre);

		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(10, 64, 97, 14);
		add(lblApellidos);
		servicio = new UsuarioService();
		servicio.consultarUsuario(usuario);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(97, 8, 122, 20);
		txtUsername.setEditable(false);
		txtUsername.setText(usuario.getUsuario());
		add(txtUsername);
		txtUsername.setColumns(10);

		txtNombre = new JTextField();
		txtNombre.setBounds(97, 36, 122, 20);
		txtNombre.setEditable(false);
		txtNombre.setText(usuario.getNombre());
		add(txtNombre);
		txtNombre.setColumns(10);

		txtApellidos = new JTextField();
		txtApellidos.setBounds(97, 61, 122, 20);
		txtApellidos.setEditable(false);
		txtApellidos.setText(usuario.getApellidos());
		add(txtApellidos);
		txtApellidos.setColumns(10);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(130, 129, 89, 23);
		add(btnSalir);
		btnSalir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				appController.volverLoginView();
			}
		});

		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(31, 129, 89, 23);
		add(btnModificar);
		btnModificar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					appController.btnModificarView(usuario);
				} catch (UsuarioException e1) {
					e1.printStackTrace();
				}
			}
		});

		initialize();
	}

	@Override
	public void initialize() { }
}
