package aplicacionUsuario.gui;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import aplicacionUsuario.App;
import aplicacionUsuario.modelo.Usuario;
import aplicacionUsuario.servicio.UsuarioException;
import aplicacionUsuario.servicio.UsuarioService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class GuardarView extends AbstractView {

	private static final long serialVersionUID = -1695354796922528508L;
	
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	
	public GuardarView(App appController, Usuario usuario) throws UsuarioException {
		setLayout(null);
		this.appController = appController;
		UsuarioService servicio = new UsuarioService();
		servicio.consultarUsuario(usuario);
		
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 11, 60, 14);
		add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 36, 60, 14);
		add(lblPassword);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 61, 60, 14);
		add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(10, 86, 60, 14);
		add(lblApellidos);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(109, 11, 110, 20);
		add(txtUsername);
		txtUsername.setText(usuario.getUsuario());
		txtUsername.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(109, 36, 110, 20);
		add(txtPassword);
		txtPassword.setText(usuario.getPassword());
		txtPassword.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(109, 61, 110, 20);
		add(txtNombre);
		txtNombre.setText(usuario.getNombre());
		txtNombre.setColumns(10);
		
		txtApellidos = new JTextField();
		txtApellidos.setBounds(109, 86, 110, 20);
		add(txtApellidos);
		txtApellidos.setText(usuario.getApellidos());
		txtApellidos.setColumns(10);
		
		
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(130, 127, 89, 23);
		add(btnVolver);
		btnVolver.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					appController.volverModificarView(usuario);
				} catch (UsuarioException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(31, 127, 89, 23);
		add(btnGuardar);
		btnGuardar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Usuario usuarioNuevo = new Usuario();
					usuarioNuevo.setUsuario(txtUsername.getText());
					String password = String.valueOf(txtPassword.getPassword());
					usuarioNuevo.setPassword(password);
					usuarioNuevo.setNombre(txtNombre.getText());
					usuarioNuevo.setApellidos(txtApellidos.getText());
					
					if (usuarioNuevo.getUsuario().isEmpty() || usuarioNuevo.getUsuario().isBlank()
							|| usuarioNuevo.getPassword().isEmpty() || usuarioNuevo.getPassword().isBlank()
							|| usuarioNuevo.getNombre().isEmpty() || usuarioNuevo.getNombre().isBlank()
							|| usuarioNuevo.getApellidos().isEmpty() || usuarioNuevo.getApellidos().isBlank()) {
						JOptionPane.showMessageDialog(null, "Los datos no pueden estar en blanco.");
					}
					else {
						servicio.guardarDatosNuevos(usuarioNuevo);
						JOptionPane.showMessageDialog(null, "Los datos se han guardado correctamente.");
						appController.volverModificarView(usuarioNuevo);
					}
				} catch (UsuarioException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public void initialize() { }
}
