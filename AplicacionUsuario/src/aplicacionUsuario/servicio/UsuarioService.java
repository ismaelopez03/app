package aplicacionUsuario.servicio;

import java.sql.Connection;

import aplicacionUsuario.dao.ConnectionProvider;
import aplicacionUsuario.dao.UsuarioDao;
import aplicacionUsuario.modelo.Usuario;

public class UsuarioService {

	public UsuarioService() {
	}

	public Boolean btnLogin(Usuario usuario) throws UsuarioException {
		Connection conn = null;
		try {
			conn = new ConnectionProvider().getNewConnection();
			UsuarioDao usuarioDao = new UsuarioDao();

			return (usuarioDao.validarLogin(conn, usuario));
		}

		catch (Exception e) {
			throw new UsuarioException(e.getMessage());
		}
	}

	public Usuario consultarUsuario(Usuario usuario) throws UsuarioException {
		Connection conn = null;

		try {
			conn = new ConnectionProvider().getNewConnection();
			UsuarioDao usuarioDao = new UsuarioDao();

			usuario = usuarioDao.consultarUsuarioDao(conn, usuario);

		} catch (Exception e) {
			throw new UsuarioException(e.getMessage());
		}
		return usuario;
	}

	public void guardarDatosNuevos(Usuario usuario) throws UsuarioException {
		Connection conn = null;

		try {
			conn = new ConnectionProvider().getNewConnection();
			UsuarioDao usuarioDao = new UsuarioDao();
			usuarioDao.guardarDatosDao(conn, usuario);

		} catch (Exception e) {
			throw new UsuarioException("Error guardando los nuevos datos.");
		}
	}

}
