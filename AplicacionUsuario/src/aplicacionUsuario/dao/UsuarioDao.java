package aplicacionUsuario.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import aplicacionUsuario.modelo.Usuario;
import aplicacionUsuario.servicio.UsuarioException;

public class UsuarioDao {

	public UsuarioDao() {
	}
	

	public Boolean validarLogin(Connection conn, Usuario usuario) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			
			stmt = conn.prepareStatement(
			"SELECT * FROM usuarios WHERE UPPER(usuario) = UPPER(?) AND UPPER(password) = UPPER(?)");
			stmt.setString(1, usuario.getUsuario());
			stmt.setString(2, usuario.getPassword());
			
			rs = stmt.executeQuery();
			while (rs.next()) {
				
				if (usuario.getUsuario().equals(rs.getString("usuario"))
						&& usuario.getPassword().equals(rs.getString("password"))) {
					return true;
				}
			}
			return false;
		} 
		
		catch (Exception e) {
			throw new SQLException("Error al validar el usuario y la contraseña.");
		}
		
		finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	public Usuario consultarUsuarioDao(Connection conn, Usuario usuario) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("SELECT * FROM usuarios WHERE UPPER(usuario) = UPPER(?)");
			stmt.setString(1, usuario.getUsuario());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				usuario.setUsuario(rs.getString("usuario"));
				usuario.setId(rs.getInt("id"));
				usuario.setPassword(rs.getString("password"));
				usuario.setNombre(rs.getString("nombre"));
				usuario.setApellidos(rs.getString("apellidos"));
			}
		}
		
		catch (Exception e) {
			throw new SQLException("Error al consultar el usuario.");
		}
		
		finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return usuario;
	}


	public void guardarDatosDao(Connection conn, Usuario usuario) throws UsuarioException, SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("UPDATE usuarios SET usuario = (?), password = (?), nombre = (?), apellidos = (?) WHERE UPPER(usuario) = UPPER(?)");
			stmt.setString(1, usuario.getUsuario());
			stmt.setString(2, usuario.getPassword());
			stmt.setString(3, usuario.getNombre());
			stmt.setString(4, usuario.getApellidos());
			stmt.setString(5, usuario.getUsuario());
			
			stmt.executeUpdate();
		} 
		
		catch (Exception e) {
			throw new UsuarioException("Error al actualizar en la BBDD.");
		}
		
		finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
}
