package aplicacionUsuario.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
	public Connection getNewConnection() throws SQLException {
		String usuario = "login";
		String password = "login";

		String url = "jdbc:mariadb://localhost:3306/loginapp";
		String driverClass = "org.mariadb.jdbc.Driver";
		
//		String driverClass = "oracle.jdbc.driver.OracleDriver";
//		String url = "jdbc:oracle:thin:@//172.16.39.200:1521/XE";

		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revisa su configuración");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
}
